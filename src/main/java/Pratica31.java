
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vagner
 */
public class Pratica31 {
    private static Date inicio = new Date();
    private static Date fim;
    private static String meuNome = "vAGNER gONÇALVES lEITÃO";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1965,Calendar.AUGUST,27);
    public static void main(String[] args) {
        
        
        String p1Nome, p2Nome, p3Nome;
        String inicio1 = "Resultado Item 5 - Programação: Operações com strings e datas\n";
        String inicio2 = "Resultado Item 6 - Programação: Operações com strings e datas\n";
        String inicio3 = "Resultado Item 7 - Programação: Operações com strings e datas\n";
        String inicio4 = "Resultado Item 8 - Programação: Operações com strings e datas\n";
        
        GregorianCalendar agora = new GregorianCalendar();
        Date valNasc=dataNascimento.getTime(), valAgora=agora.getTime();
        long valMs1=valNasc.getTime(), valMs2=valAgora.getTime(), totMs, totDias;
        totDias=(valMs2-valMs1)/86400000;
        
        p1Nome = meuNome.substring(0,1).toUpperCase().charAt(0)+". ";
        p2Nome = meuNome.substring(0,8).toUpperCase().charAt(7)+".";
        p3Nome = meuNome.substring(0,18).toUpperCase().charAt(17)+meuNome.substring(18).toLowerCase()+", ";
        System.out.println(inicio1+meuNome.toUpperCase()+"\n");
        System.out.println(inicio2+p3Nome+p1Nome+p2Nome+"\n");
        System.out.println(inicio3+totDias+" dias.\n");
        long valMs;
        fim= new Date();
        valMs = fim.getTime() - inicio.getTime();
        System.out.println(inicio4+valMs+" milisegundos.\n");
    }
}
